#!/usr/bin/env python3
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate
from keras.models import Model
import tensorflow as tf
from build_model import build_model, build_model_detect
from keras.utils.vis_utils import plot_model
from keras.callbacks import EarlyStopping, ModelCheckpoint, LearningRateScheduler
from keras.utils import to_categorical
import keras
from pickle import load
import matplotlib.pyplot as plt
import itertools as it

print('Training of detect network')

# # Settings
# ###############################################################

net_dir = os.path.expanduser('~/new_network')
train_dir = os.path.expanduser('~/new_network/training')

settings = np.load(train_dir+'/settings.npy', allow_pickle=True).item() 


settings['layer_type'] = 'conv'  #'conv' or 'dense' or 'dense+conv'
settings['size'] = 'big'  #'small' or 'big'  
settings['n_epochs'] = 100


settings_name = settings['layer_type'] + '_' + str(settings['n_epochs']) + 'epochs_' + settings['size'] + '_'\
                + settings['dropout'] + '_' + settings['augment'] + '_' + settings['mode']

print(settings)
print(settings_name)
np.save(train_dir+'/det_settings.npy', settings) 

# # # Build model
# # ###############################################################
model_detect = build_model_detect(784,settings['layer_type'],settings['size']) # building 21 architecture for the detection model
plot_model(model_detect, to_file=os.path.expanduser(net_dir)+'/model_plots/det_'+settings['layer_type']+'_'+settings['size']+'.png', show_shapes=True, show_layer_names=True)

# # Training specifics
# ###############################################################

# Specify optimisers and loss functions for each output
def scheduler(epoch, lr):
    if epoch < 40:
        return lr
    else:
        return lr * tf.math.exp(-0.01)

lr_schedule = LearningRateScheduler(scheduler, verbose=1)

optimizer = tf.keras.optimizers.Adam(
    learning_rate=1e-3, beta_1=0.9, beta_2=0.999, epsilon=1e-06, amsgrad=False,
    name='Adam'
)

model_detect.compile(optimizer=optimizer, 
        loss='categorical_crossentropy',
        metrics=['categorical_accuracy','Precision','Recall'])



# # # Train the model, and save metrics into history object
# # ###############################################################
earlyStopping = EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='min') #patience may be more suitable between 5-10
mcp_save = ModelCheckpoint(net_dir + '/models/det_' + settings_name , 
                    save_best_only=True,
                    monitor='val_loss', 
                    mode='min', 
                    save_weights_only=False)




# ###############################################################
# Anomaly finding

# ###############################################################
print('Training of detection network')

x_train = np.load(train_dir+'/x_train_DET.npy')
y_train = np.load(train_dir+'/y_train_DET.npy')
print("x_train and y_train shapes: ",x_train.shape, y_train.shape)

# # MSE of the readings
# print('--------SQUARED ERROR----------')
# se = np.subtract(x_train[:, 1:],y_train[:, 1:])**2
# X_det = se

print('--------IF AE RECONSTRUCT DOESNT WORK----------')
X_det = x_train
Y_det = to_categorical(y_train-1, num_classes=4) #convert labels 0,1,2,3 into one-hot vectors


# ###############################################################
# Train detector part


history = model_detect.fit(X_det,Y_det, 
                shuffle=True, epochs=settings['n_epochs'], verbose=2, batch_size=6000,          # Set verbose to 0 to supress all prints
                validation_split = 0.2,
                callbacks=[earlyStopping, mcp_save, lr_schedule],
                max_queue_size=1,
                use_multiprocessing=False, 
                workers=1)

np.save(os.path.expanduser(net_dir) + '/history/det_' + settings_name, history.history)
# ###############################################################
