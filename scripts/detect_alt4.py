#!/usr/bin/env python3
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import keras
from keras.metrics import CategoricalAccuracy, Precision, Recall
from pickle import load
from keras.utils import to_categorical
from help_functions import plot_multi_samples

print("Detector alternative 4")

##########################################
# settings
##########################################

data_dir = os.path.expanduser('~/new_network/training')
network_dir = os.path.expanduser('~/new_network')


settings = np.load(data_dir+'/settings.npy', allow_pickle=True).item() 

dropout = settings['dropout']
augment = settings['augment']
mode = settings['mode']
# layer_type = settings['layer_type'] 
# size = settings['size'] 
# bn = settings['bn']
# n_epochs = settings['n_epochs']
# loss_f = settings['loss_f']
save = True

# settings_name = layer_type+ '_' + loss_f + '_' \
                # + size + '_BN' + str(bn) + '_' \
                # + str(n_epochs) + 'epochs_' \
                # + dropout + '_' + augment + '_' \
                # + mode

print(settings)

# # Load AE model
# model = keras.models.load_model(network_dir + '/models/' + settings_name)

##########################################
# Load data
##########################################
x_val = np.load(data_dir + '/x_train_AE.npy')
y_val = np.load(data_dir + '/y_train_AE.npy')
y_val_det =  to_categorical(x_val[:, :1]-1, num_classes=4) # convert labels 0,1,2,3 into one-hot vectors

print(x_val.shape, y_val_det.shape)


# ##########################################
# # Predict data
# ##########################################
# print('--------Reconstruct samples----------')

# rec_samples = model.predict(x_val[:,1:])

# print("min of 10 first rows",np.amin(rec_samples[0:10, :],1))
# print("max of 10 first rows",np.amax(rec_samples[0:10, :],1))

rec_samples = y_val # comment out when AE is done

# plot_multi_samples([y_val[0,1:], x_val[0,1:]], 
#                     ['y samples', 'x samples'], save=False)
# plot_multi_samples([y_val[0,1:], x_val[0,1:]], 
#                     ['y samples', 'x samples'], save=False)

# MSE of the readings
print('--------MSE----------')
cam_ind, lid_ind = slice(1,425), slice(425,None)
mse_cam = ((x_val[:,cam_ind] - rec_samples[:,cam_ind])**2).mean(axis=1)
mse_lid = ((x_val[:,lid_ind] - rec_samples[:,lid_ind])**2).mean(axis=1)
print("mse cam and lid shapes: ",mse_cam.shape, mse_lid.shape)
mse = np.dstack((mse_cam,mse_lid))
mse = mse[0]
print("mse shape: ",mse.shape)


# Label depending on range MSE falls within
x_val_det = []
for row in mse:
    temp = 0
    if row[0] >= 1:
        temp = 1 # cam error
    if row[1] >= 1:
        temp += 2 # lid error or both errors
    x_val_det.append(temp)

x_val_det = to_categorical(x_val_det, num_classes=4)

print("yval det\n", y_val_det[0:10], "\nxval det\n", x_val_det[0:10])

##########################################
# Performance
##########################################
print('--------Performance----------')
# Categorical accuracy
cat_acc = CategoricalAccuracy()
cat_acc.update_state(y_val_det, x_val_det)
print("Categorical Accuracy: ",cat_acc.result().numpy())

# Precision
precision = Precision()
precision.update_state(y_val_det, x_val_det)
print("Precision: ", precision.result().numpy())

# Recall
recall = Recall()
recall.update_state(y_val_det, x_val_det)
print("Recall", recall.result().numpy())
