import numpy as np
from help_functions import augment_data, plot_multi_samples, dropout_data, load_n_batches, clean_data, plot_one_sample
import os
import math

lidar_angle_min = -3.1241390705108643
lidar_angle_increment = 0.01745329238474369
lidar_angle_max = 3.1415927410125732


camera_angle_min = -0.7669617533683777
camera_angle_max = 0.7851662635803223
cam_angle_increment = 0.0036693334113806486


cam_angles = np.arange(camera_angle_min, camera_angle_max + cam_angle_increment, cam_angle_increment)
cam_grid = np.arange(camera_angle_min - 0.5*cam_angle_increment, camera_angle_max + cam_angle_increment, cam_angle_increment)


lid_angles = np.arange(lidar_angle_min, lidar_angle_max + lidar_angle_increment, lidar_angle_increment)
lid_grid = np.arange(lidar_angle_min - 0.5 * lidar_angle_increment, lidar_angle_max + lidar_angle_increment, lidar_angle_increment)



cam = np.random.randint(1, 5, (424, ))
lid = np.random.randint(1, 5, (360, ))

print(cam_angles.shape, cam_grid.shape, cam.shape)

# print('cam ang', cam_angles[0:4])
# print('cam grid', cam_grid[0:4])
# print('lid ang', lid_angles[0:4])
# print('lid grid', lid_grid[0:4])


digi_cam = np.digitize(cam_angles, lid_grid)
digi_lid = np.digitize(lid_angles, lid_grid)


bin_cam = [np.nanmean(cam[digi_cam == i]) for i in range(1, len(lid_grid))]
bin_lid = [np.nanmean(lid[digi_lid == i]) for i in range(1, len(lid_grid))]

print(bin_lid)

cam_inds = list(np.where(np.isfinite(bin_cam))[0])

print('---------')
# print(bin_cam[cam_inds[0:3], bin_lid[cam_inds[0:3]])

print('---------')

print(bin_cam[cam_inds[0]], bin_lid[cam_inds[0]])

print('---------')
# print(bin_cam)

print('---------')

bin_lid[cam_inds[0]] = bin_cam[cam_inds[0]]

# print(bin_lid)
print(bin_cam[cam_inds[0]], bin_lid[cam_inds[0]])





