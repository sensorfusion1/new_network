#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import keras
import time
import tensorflow as tf
from pickle import load
from sklearn.preprocessing import RobustScaler, Normalizer
from help_functions import plot_multi_samples, plot_history
import pathlib
from keras.utils.vis_utils import plot_model


print('Loading net for visualization')

##########################################
# settings
##########################################

data_dir = os.path.expanduser('~/new_network/training')
network_dir = os.path.expanduser('~/new_network')


settings = np.load(data_dir+'/settings.npy', allow_pickle=True).item() 

dropout = settings['dropout']
augment = settings['augment']
mode = settings['mode']
layer_type = settings['layer_type'] 
size = settings['size'] 
bn = settings['bn']
n_epochs = settings['n_epochs']
loss_f = settings['loss_f']
save = True

settings_name = settings['size'] + '_' \
                + settings['filters'] + '_' \
                + str(settings['n_epochs']) + 'epochs_' \
                + settings['loss_f'] + '_' \
                + settings['lr_sch'] + '_' \
                + 'BN' + str(settings['bn']) + '_' \
                + settings['mode'] + '_' \
                + settings['dropout'] + '_' \
                + settings['augment']

# settings_name = settings['size'] + '_' \
#                 + settings['filters'] + '_' \
#                 + settings['mode'] + '_' \
#                 + settings['lr_sch'] + '_' \
#                 + settings['loss_f'] \
#                 + '_BN' + str(settings['bn']) + '_' \
#                 + str(settings['n_epochs']) \
#                 + 'epochs_' \
#                 + settings['dropout'] + '_' \
#                 + settings['augment']

# settings_name = layer_type+ '_' + loss_f + '_' \
#                 + size + '_BN' + str(bn) + '_' \
#                 + str(n_epochs) + 'epochs_' \
#                 + dropout + '_' + augment + '_' \
#                 + mode





print(settings)
print("setting_name: ",settings_name)


save_dir = network_dir + '/plots/' + settings_name 
pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True) 




i_l = 1
i_c = 425

##########################################
# Load data
##########################################
x_val = np.load(data_dir + '/orig_data.npy')
y_val = np.load(data_dir + '/y_data.npy')


print(x_val.shape, y_val.shape)

print('--------Validation entries----------')
print(x_val[0, 0:5])
print(y_val[0, 0:5])



# Load model #
##########################################
model = keras.models.load_model(network_dir + '/models/' + settings_name)
history = np.load(network_dir + '/history/' + settings_name + '.npy',allow_pickle='TRUE').item()

plot_model(model, to_file=save_dir+'/model_plot.png', show_shapes=True, show_layer_names=True)



##########################################
# Predict data
##########################################
print('--------Reconstruct samples----------')

# np.random.seed(21)

selected_samples = np.random.randint(0, 500, (5, ))
x_samples = x_val[selected_samples, :]
y_samples = y_val[selected_samples, :]

print(selected_samples, x_samples.shape)
rec_samples = model.predict(x_samples[:,1:])

print(rec_samples[0:2, 0:4])


plot_multi_samples([y_samples[:,1:], x_samples[:,1:], rec_samples], 
                    ['y samples', 'x samples', 'reconstructed'],
                    save_dir, '/reconstructed', save)


# ##########################################
# # Plot the metrics
# ##########################################

print('--------History----------')
print(history.keys())

plot_history(history, 
            ['loss', 'mean_squared_error', 'cosine_similarity', 'mean_absolute_error', 'kullback_leibler_divergence', 'logcosh'],
            save_dir, '/losses', save)

#, 'cosine_similarity', kullback_leibler_divergence
