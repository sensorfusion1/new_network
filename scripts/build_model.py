#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate, Conv1D, MaxPooling1D, UpSampling1D, Conv1DTranspose, Flatten, BatchNormalization, Dropout
from keras.models import Model
import tensorflow as tf



from keras.utils.vis_utils import plot_model

def build_model(layer_type, size, bn, input_dim):
    ## Camera branch

    if layer_type == 'conv':
        if size == 'big': dims = [[5, 3], [5, 3]]*8
        if size == 'small': dims = [[5, 3], [5, 3]]
        start = [3, 3]

        input_scans = Input(shape=(input_dim, ))
        output_scans = tf.keras.layers.Reshape((input_dim, 1), input_shape=(input_dim,))(input_scans)
        output_scans = Conv1D(start[0], start[1], padding='same', activation="relu", use_bias=True)(output_scans)
        output_scans = UpSampling1D(2)(output_scans)

        for dim in dims:
            if bn: output_scans = BatchNormalization()(output_scans)
            output_scans = Conv1D(dim[0], dim[1], padding='same', activation="relu", use_bias=True)(output_scans)
            # output_scans = UpSampling1D(2)(output_scans)

        for dim in reversed(dims):
            if bn: output_scans = BatchNormalization()(output_scans)
            output_scans = Conv1D(dim[0], dim[1], padding='same', activation="relu", use_bias=True)(output_scans)
            # output_scans = MaxPooling1D(2, padding='same')(output_scans)

        if bn: output_scans = BatchNormalization()(output_scans)
        output_scans = Conv1D(start[0], start[1], padding='same', activation="relu", use_bias=True)(output_scans)
        output_scans = MaxPooling1D(2, padding='same')(output_scans)
        output_scans = Flatten()(output_scans)
        output_scans = Dense(input_dim, activation="linear",use_bias=True)(output_scans) 

        model = Model(inputs=input_scans, outputs=output_scans)

    if layer_type == 'dense':
        if size == 'big': dims = [1000, 1200, 1500]
        if size == 'small': dims = [1300]
        start = 800

        input_scans = Input(shape=(input_dim, ))
        output_scans = Dense(start, activation="relu", use_bias=True)(input_scans)

        for dim in dims:
            if bn: output_scans = BatchNormalization()(output_scans)
            output_scans = Dense(dim, activation="relu", use_bias=True)(output_scans)

        for dim in reversed(dims):
            if bn: output_scans = BatchNormalization()(output_scans)
            output_scans = Dense(dim, activation="relu", use_bias=True)(output_scans)
     
        if bn: output_scans = BatchNormalization()(output_scans)
        output_scans = Dense(start, activation="relu", use_bias=True)(output_scans)
        output_scans = Dense(input_dim, activation="linear",use_bias=True)(output_scans) 

        model = Model(inputs=input_scans, outputs=output_scans)


    return model


def build_model_detect(input_dim,layer_type,size):
    input_scans = Input(shape=(input_dim, 1))
    drop_out_rate = 0.5
    if layer_type == 'dense':
        if size == 'small':
            output_scans = Flatten()(input_scans)
            output_scans = Dense(512, activation='relu',use_bias=True)(output_scans)
            output_scans = Dense(128, activation='relu',use_bias=True)(output_scans)
            output_scans = BatchNormalization()(output_scans)
            output_scans = Dropout(0.5)(output_scans)
            output_scans = Dense(4, activation="softmax",use_bias=True)(output_scans)
        elif size == 'big':
            output_scans = Flatten()(input_scans)
            output_scans = Dense(512, activation='relu',use_bias=True)(output_scans)
            output_scans = Dense(256, activation='relu',use_bias=True)(output_scans)
            output_scans = Dense(128, activation='relu',use_bias=True)(output_scans)
            output_scans = Dense(64, activation='relu',use_bias=True)(output_scans)
            output_scans = Dense(32, activation='relu',use_bias=True)(output_scans)
            output_scans = Dense(4, activation="softmax",use_bias=True)(output_scans) #https://glassboxmedicine.com/2019/05/26/classification-sigmoid-vs-softmax/
    
    elif layer_type == 'conv':
        if size == 'small':
            # x = Conv1D(16, 11, padding='valid', activation='relu', strides=1)(input_scans)
            # x = MaxPooling1D(2)(x)
            # x = Dropout(drop_out_rate)(x)
            # x = Conv1D(128, 5, padding='valid', activation='relu', strides=1)(x)
            # x = MaxPooling1D(2)(x)
            # x = Flatten()(x)
            # output_scans = Dense(1024, activation='relu')(x)
            # output_scans = Dense(4, activation='softmax')(output_scans)

            # https://www.pyimagesearch.com/2018/05/07/multi-label-classification-with-keras/
            output_scans = Conv1D(32, 3, padding="same", activation='relu')(input_scans)
            output_scans = BatchNormalization()(output_scans)
            output_scans = MaxPooling1D(2)(output_scans)
            output_scans = Dropout(0.25)(output_scans)
            output_scans = Conv1D(128, 3, padding="same", activation='relu')(output_scans)
            output_scans = BatchNormalization()(output_scans)
            output_scans = MaxPooling1D(2)(output_scans)
            output_scans = Dropout(0.25)(output_scans)
            output_scans = Flatten()(output_scans)
            output_scans = Dense(1024, activation='relu',use_bias=True)(output_scans)
            output_scans = BatchNormalization()(output_scans)
            output_scans = Dropout(0.5)(output_scans)
            output_scans = Dense(4, activation='softmax',use_bias=True)(output_scans)

        elif size == 'big':
            # x = Conv1D(8, 11, padding='valid', activation='relu', strides=1)(input_scans)
            # x = MaxPooling1D(2)(x)
            # x = Dropout(drop_out_rate)(x)
            # x = Conv1D(16, 7, padding='valid', activation='relu', strides=1)(x)
            # x = MaxPooling1D(2)(x)
            # x = Dropout(drop_out_rate)(x)
            # x = Conv1D(32, 5, padding='valid', activation='relu', strides=1)(x)
            # x = MaxPooling1D(2)(x)
            # x = Dropout(drop_out_rate)(x)
            # x = Conv1D(64, 5, padding='valid', activation='relu', strides=1)(x)
            # x = MaxPooling1D(2)(x)
            # x = Dropout(drop_out_rate)(x)
            # x = Conv1D(128, 3, padding='valid', activation='relu', strides=1)(x)
            # x = MaxPooling1D(2)(x)
            # output_scans = Flatten()(x)
            # output_scans = Dense(4, activation='softmax')(output_scans)

            ### https://www.pyimagesearch.com/2018/05/07/multi-label-classification-with-keras/
            # output_scans = Conv1D(32, 3, padding="same", activation='relu')(input_scans)
            # output_scans = BatchNormalization()(output_scans)
            # output_scans = MaxPooling1D(3)(output_scans)
            # output_scans = Dropout(0.25)(output_scans)

            # output_scans = Conv1D(64, 3, padding="same", activation='relu')(output_scans)
            # output_scans = BatchNormalization()(output_scans)
            # output_scans = Conv1D(64, 3, padding="same", activation='relu')(output_scans)
            # output_scans = BatchNormalization()(output_scans)
            # output_scans = MaxPooling1D(2)(output_scans)
            # output_scans = Dropout(0.25)(output_scans)

            # output_scans = Conv1D(128, 3, padding="same", activation='relu')(output_scans)
            # output_scans = BatchNormalization()(output_scans)
            # output_scans = Conv1D(128, 3, padding="same", activation='relu')(output_scans)
            # output_scans = BatchNormalization()(output_scans)
            # output_scans = MaxPooling1D(2)(output_scans)
            # output_scans = Dropout(0.25)(output_scans)

            # output_scans = Flatten()(output_scans)
            # output_scans = Dense(1024, activation='relu')(output_scans) #4096, 512, 64, 4
            # output_scans = BatchNormalization()(output_scans)
            # output_scans = Dropout(0.5)(output_scans)
            # output_scans = Dense(4, activation='softmax')(output_scans)

            ### https://medium.com/analytics-vidhya/unsupervised-learning-and-convolutional-autoencoder-for-image-anomaly-detection-b783706eb59e

            x = Conv1D(32, 3, activation='relu', padding='same')(input_scans)
            x = MaxPooling1D(2, padding='same')(x)
            x = Conv1D(32, 3, activation='relu', padding='same')(x)
            x = MaxPooling1D(2, padding='same')(x)
            x = Conv1D(32, 3, activation='relu', padding='same')(x)
            encoded = MaxPooling1D(2, padding='same')(x)

            x = Conv1D(32, 3, activation='relu', padding='same')(encoded)
            x = UpSampling1D(2)(x)
            x = Conv1D(32, 3, activation='relu', padding='same')(x)
            x = UpSampling1D(2)(x)
            x = Conv1D(32, 3, activation='relu')(x)
            x = UpSampling1D(2)(x)
            decoded = Conv1D(3, 3, activation='sigmoid', padding='same')(x)
            output_scans = Flatten()(decoded)
            output_scans = Dense(4, activation='softmax',use_bias=True)(output_scans)
            
    elif layer_type == 'dense+conv':
        if size == 'small':
            x = Conv1D(32, 3, padding='valid', activation='relu', strides=1)(input_scans)
            x = MaxPooling1D(2)(x)
            x = Dropout(drop_out_rate)(x)
            x = Conv1D(64, 3, padding='valid', activation='relu', strides=1)(x)
            x = MaxPooling1D(2)(x)
            x = Flatten()(x)
            x = Dense(64, activation='relu')(x)
            x = Dropout(drop_out_rate)(x)
            x = Dense(16, activation='relu')(x)
            output_scans = Dropout(drop_out_rate)(x)
            output_scans = Dense(4, activation='softmax')(output_scans)
        elif size == 'big':
            x = Conv1D(8, 11, padding='valid', activation='relu', strides=1)(input_scans)
            x = MaxPooling1D(2)(x)
            x = Dropout(drop_out_rate)(x)
            x = Conv1D(16, 7, padding='valid', activation='relu', strides=1)(x)
            x = MaxPooling1D(2)(x)
            x = Dropout(drop_out_rate)(x)
            x = Conv1D(32, 5, padding='valid', activation='relu', strides=1)(x)
            x = MaxPooling1D(2)(x)
            x = Dropout(drop_out_rate)(x)
            x = Conv1D(64, 5, padding='valid', activation='relu', strides=1)(x)
            x = MaxPooling1D(2)(x)
            x = Dropout(drop_out_rate)(x)
            x = Conv1D(128, 3, padding='valid', activation='relu', strides=1)(x)
            x = MaxPooling1D(2)(x)
            x = Flatten()(x)
            x = Dense(256, activation='relu')(x)
            x = Dropout(drop_out_rate)(x)
            x = Dense(128, activation='relu')(x)
            output_scans = Dropout(drop_out_rate)(x)
            output_scans = Dense(4, activation='softmax')(output_scans)


    model = Model(inputs=input_scans, outputs=output_scans)


    return model
