#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import keras
import time
from pickle import load
from sklearn.preprocessing import RobustScaler, Normalizer
from help_functions import plot_multi_samples, plot_history
import pathlib


print('Loading detect net for visualization')

##########################################
# settings
##########################################

data_dir = os.path.expanduser('~/new_network/training')
network_dir = os.path.expanduser('~/new_network')
train_dir = os.path.expanduser('~/new_network/training')


settings = np.load(data_dir+'/det_settings.npy', allow_pickle=True).item() 




save = True

settings_name = settings['layer_type'] + '_' + str(settings['n_epochs']) + 'epochs_' + settings['size'] + '_'\
                + settings['dropout'] + '_' + settings['augment'] + '_' + settings['mode']
print(settings,"\n",settings_name)

save_dir = network_dir + '/plots/det_' + settings_name 
pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True) 

##########################################
# Load data
##########################################
x_val = np.load(train_dir+'/x_train_AE.npy')
y_val = np.load(train_dir+'/y_train_AE.npy')


print(x_val.shape, y_val.shape)

print('--------Validation entries----------')
print(x_val[0, 0:5])
print(y_val[0, 0:5])



# Load model #
##########################################
model = keras.models.load_model(network_dir + '/models/det_' + settings_name)
history = np.load(network_dir + '/history/det_' + settings_name + '.npy',allow_pickle='TRUE').item()





##########################################
# Predict data
##########################################
# print('--------Reconstruct samples----------')

# # np.random.seed(21)

# selected_samples = np.random.randint(0, 500, (5, ))
# x_samples = x_val[selected_samples, :]
# y_samples = y_val[selected_samples, :]

# print(selected_samples, x_samples.shape)
# rec_samples = model.predict(x_samples[:,1:])

# print(rec_samples[0:2, 0:4])


# plot_multi_samples([y_samples[:,1:], x_samples[:,1:], rec_samples], 
#                     ['y samples', 'x samples', 'reconstructed'],
#                     save_dir, '/reconstructed', save)


# ##########################################
# # Plot the metrics
# ##########################################

print('--------History----------')

plot_history(history, 
            ['loss', "categorical_accuracy","precision","recall"],
            save_dir, '/losses', save)

print(history.keys())
