import numpy as np
import os
import matplotlib.pyplot as plt
import math
import itertools as it

rads_lid = np.arange(-3.1241390705108643, 3.1415927410125732+0.01745329238474369 ,  0.01745329238474369)
rads_cam = np.arange(-0.7669617533683777, 0.7851662635803223+0.0036693334113806486 ,  0.0036693334113806486)

def load_n_batches(from_dir,start_batch, n_batches):
    data = np.load(from_dir+str(start_batch)+'.npy')
    labels = np.load(from_dir+str(start_batch)+'_labelled.npy')
    data[:,2] = labels
    data = data[:,2:]

    for i in range(start_batch+1, n_batches+1):
        temp = np.load(from_dir+str(i)+'.npy')
        labels = np.load(from_dir+str(i)+'_labelled.npy')
        temp[:,2] = labels
        temp = temp[:,2:]
        data = np.concatenate((data, temp))


    return data

###############

def augment_data(arr, noise, augment):
    exp_arr = []
    for i in range(len(noise)): #-1
        noise_arr = np.random.normal(noise[i][0], noise[i][1], size=arr.shape)
        if augment == 'AE':
            noise_arr[:,0] = 0
        noise_arr = noise_arr + arr
        exp_arr.append(noise_arr)

    exp_arr = np.vstack(exp_arr)
    return exp_arr

###############

def split_data(div, arr):
    train = []
    val = []
    test = []
    for i in range(len(arr)):
        n = arr[i].shape[0]
        temp = np.split(arr[i], [int(n*div[0]), int(n*div[1])])
        train.append(temp[0])
        val.append(temp[1])
        test.append(temp[2])

    train = np.vstack(train)
    val = np.vstack(val)
    test = np.vstack(test)
    return train, val, test

###############


# def clean_data(arr):
#     clean_arr = arr.copy()
#     for row in range(arr.shape[0]):
#         ins = np.where(arr[row,:] > 20) 
#         for j in range(len(ins[0])):
#             clean_arr[row, ins[0][j]] = clean_arr[row, ins[0][j]-1]
            
   
#     return clean_arr


def clean_data(arr):
    clean_arr = arr.copy()
    for row in range(arr.shape[0]):
        ins_lid = np.where(arr[row,:] > 20)
        for j in ins_lid[0]:
            clean_arr[row, j] = clean_arr[row, j-1]
        
        ins_cam = np.where(arr[row,:424] < 0.15)
        for i in ins_cam[0]:
            clean_arr[row, i+1] = clean_arr[row, i]

    return clean_arr



###############

def plot_one_sample(sample, title):



    _, ax = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(12,10))
    #ax.set_ylim(0,4)
    plt.subplots_adjust(bottom=0.2, top=0.8)


    plt.plot(rads_cam, sample[:424], lw=2)
    plt.plot(rads_lid, sample[424:], lw=2)
    plt.title(title)
    plt.show()

###############

def plot_multi_samples(arr, entries, save_dir='dir', title='Figure', save = False):

    rows = len(arr)
    samples = arr[0].shape[0]
    
    if samples == 784:
        _, axs = plt.subplots(rows, 1, figsize=(15,10), subplot_kw=dict(projection="polar"))
        for row in range(rows):
            axs[row].set_title(entries[row], x=-0.3, y=0.5, rotation = 90)
            axs[row].plot(rads_cam, arr[row][:424])
            axs[row].plot(rads_lid, arr[row][424:])
            axs[row].legend(['camera', 'lidar'], loc=3)
            axs[row].set_ylim([0,4])


    elif rows == 1:
        _, axs = plt.subplots(rows, samples, figsize=(15,10), subplot_kw=dict(projection="polar"))
        for row in range(rows):
            axs[0].set_title(entries[row], x=-0.3, y=0.5, rotation = 90)

            for i in range(samples): 
                axs[i].plot(rads_cam, arr[row][i, :424])
                axs[i].plot(rads_lid, arr[row][i, 424:])
                axs[i].legend(['camera', 'lidar'], loc=3)
                axs[i].set_ylim([0,4])


    else:
        _, axs = plt.subplots(rows, samples, figsize=(15,10), subplot_kw=dict(projection="polar"))
        for row in range(rows):
            axs[row, 0].set_title(entries[row], x=-0.3, y=0.5, rotation = 90)

            for i in range(samples): 
                axs[row, i].plot(rads_cam, arr[row][i, :424])
                axs[row, i].plot(rads_lid, arr[row][i, 424:])
                axs[row, i].legend(['camera', 'lidar'], loc=3)
                axs[row, i].set_ylim([0,4])
        

    if save:
        plt.savefig(save_dir+title+'.jpg')
    else: 
        plt.show()

###############


def plot_history(arr, entries, save_dir, title, save = False):

    per_row = 2
  
    n_plots = len(entries)
    rows = int(np.rint(n_plots/per_row))
    _, axs = plt.subplots(rows, per_row, figsize=(15,10))

    options = range(rows)
    opt2 = range(per_row)
    ind = list(it.product(options, opt2))
    print(ind)
    for i in range(n_plots):
        if rows == 1:
            axs[i].plot(arr[entries[i]])
            axs[i].plot(arr['val_'+entries[i]])
            axs[i].legend(['train', 'val'])
            axs[i].set_title([entries[i]])
            # axs[i].set_ylim([np.min(arr['val_'+entries[i]])-0.05*np.mean(arr['val_'+entries[i]]), np.mean(arr['val_'+entries[i]])])
        else:
            axs[ind[i][0],ind[i][1]].plot(arr[entries[i]])
            axs[ind[i][0],ind[i][1]].plot(arr['val_'+entries[i]])
            axs[ind[i][0],ind[i][1]].legend(['train', 'val'])
            axs[ind[i][0],ind[i][1]].set_title([entries[i]])
            # axs[ind[i][0],ind[i][1]].set_ylim([np.min(arr['val_'+entries[i]])-0.05*np.mean(arr['val_'+entries[i]]), np.mean(arr['val_'+entries[i]])])





    if save:
        plt.savefig(save_dir+title+'.jpg', bbox_inches='tight')
    else: 
        plt.show()


###############

def add_errors(arra, which_err):
    arr = arra.copy()

    if which_err == 'cam':
        for i in range(arr.shape[0]):
            n_infs = np.random.randint(1, 10)
            infs = np.random.randint(1, 425, (n_infs,))
            for j in range(n_infs):
                arr[i, infs[j]] = 2 * np.random.random_sample() + 4

    if which_err == 'lid':
        for i in range(arr.shape[0]):
            n_infs = np.random.randint(1, 10)
            infs = np.random.randint(425, 785, (n_infs,))
            for j in range(n_infs):
                arr[i, infs[j]] = 1e8 * np.random.random_sample()

    if which_err == 'both':
        for i in range(arr.shape[0]):
            n_infs_c = np.random.randint(1, 10)
            n_infs_l = np.random.randint(1, 10)
            infs_l = np.random.randint(425, 785, (n_infs_l,))
            infs_c = np.random.randint(1, 425, (n_infs_c,))
            

            for j in range(n_infs_c):
                arr[i, infs_c[j]] = 2 * np.random.random_sample() + 4

            for j in range(n_infs_l):
                arr[i, infs_l[j]] = 1e8 * np.random.random_sample() 
            arr[i, infs_c[0]] = 1e8 * np.random.random_sample() 
            

    return arr

###############

def dropout_data(arra, perc):
    arr = arra.copy()
    n = int(round(perc * arr.shape[1]))
    for i in range(arr.shape[0]):
        i_zeros = np.random.randint(1, 785, (n,))
        arr[i, i_zeros] = 0
    return arr







###############