import numpy as np
import os
from pickle import dump
from sklearn.preprocessing import RobustScaler, QuantileTransformer
from help_functions import clean_data_copy, plot_one_sample, plot_multi_samples, load_n_batches, augment_data, split_data, clean_data, add_errors, dropout_data

################################################
# Settings
################################################

dropout = 'None'      #['None', 'AE', 'DET', 'AEDET']
augment = 'DET'      #['None', 'AE', 'DET', 'AEDET']
mode = 'unscaled'     #['quant_input', 'unscaled', 'quant_camlid']
save = True

start_batch = 0
num_batches = 211

batch_dir = os.path.expanduser('~/labeling/batches/batch')
train_dir = os.path.expanduser('~/new_network/training')
save_dir = os.path.expanduser('~/new_network')


settings = {'dropout':dropout, 'augment':augment, 'mode':mode}

noise = [[-0.01, 0.01], [-0.01, 0.15]] #, [-0.17, 0.05]]
dropout_perc = [0.2, 0.4]

################################################
# Load data
################################################

orig_data = load_n_batches(batch_dir, start_batch, num_batches)
print('Original data', orig_data.shape)


orig_data = np.nan_to_num(orig_data, nan=1e10)

y_data = clean_data_copy(orig_data)


################################################
# Training data for AE
################################################

nc_ind = np.where(orig_data[:,0] == 1)
nc_data = orig_data[nc_ind].copy()
nc_data_y = y_data[nc_ind].copy()

print('Only nc samples: ', nc_data.shape)


# Add synthetic errors
################################################
artif_error_cam = add_errors(nc_data_y, 'cam')
artif_error_lid = add_errors(nc_data_y, 'lid')
artif_error_both = add_errors(nc_data_y, 'both')

# Correct labels
artif_error_cam[:,0] = 2
artif_error_lid[:,0] = 3
artif_error_both[:,0] = 4

# plot_multi_samples([nc_data_y[0:3,1:], orig_data[0:3, 1:], artif_error_lid[0:3, 1:], artif_error_both[0:3, 1:]])


# Concatenate to x (with errors) and y (no errors)
################################################

# x_train_AE = np.concatenate((artif_error_cam, artif_error_lid, artif_error_both))
# y_train_AE = np.concatenate((nc_data_y, nc_data_y, nc_data_y))

x_train_AE = nc_data
y_train_AE = nc_data_y


# Shuffling the samples
shuf_ind = np.random.permutation(x_train_AE.shape[0])
x_train_AE = x_train_AE[shuf_ind, :]
y_train_AE = y_train_AE[shuf_ind, :]

print('training data AE', x_train_AE.shape, y_train_AE.shape)

################################################
# Training data for detection
################################################


# x_train_DET = np.concatenate((artif_error_cam, artif_error_lid, artif_error_both, orig_data))
# y_train_DET = np.concatenate((nc_data_y, nc_data_y, nc_data_y, y_data))
# print("*** orig data first line", orig_data[0,0:5])
x_train_DET = clean_data(orig_data[:,1:])
# print("x train", x_train_DET[0,0:10])
y_train_DET = orig_data[:,0]
# print("y train",y_train_DET[0])

print('training data DET', x_train_DET.shape, y_train_DET.shape)

################################################
# Augmentation
################################################
if augment == 'AE':
    
    x_bias = x_train_AE.copy()
    y_bias = y_train_AE.copy()
    n = x_bias.shape[0]
   

    x_bias = np.tile(x_bias, (4, 1))
    y_bias = np.tile(y_bias, (4, 1))
  
    print('bias', x_bias.shape)
    
    x_bias[n:2*n, 1:425] = x_bias[n:2*n, 1:425] - 0.1
    x_bias[2*n:3*n, 1:425] = x_bias[2*n:3*n, 1:425] + 0.1
    x_bias[3*n:4*n, 1:425] = x_bias[3*n:4*n, 1:425] - 0.17
    # plot_multi_samples([x_bias[n,1:], x_bias[3*n,1:]], 
    #         ['y' + str(x_bias[0,0]),'ybias' + str(x_bias[0,0])])

    x_train_AE = x_bias
    y_train_AE = y_bias

    x_rev = x_train_AE.copy()
    y_rev = y_train_AE.copy()

    x_rev[:,1:425] = np.flip(x_rev[:,1:425], 1)
    x_rev[:,425:]  = np.flip(x_rev[:,425:], 1)

    y_rev[:,1:425] = np.flip(y_rev[:,1:425], 1)
    y_rev[:,425:]  = np.flip(y_rev[:,425:], 1)

    x_train_AE = np.vstack((x_train_AE, x_rev))
    y_train_AE = np.vstack((y_train_AE, y_rev))

    x_train_AE_aug = augment_data(x_train_AE, noise, augment)
    x_train_AE = np.vstack((x_train_AE, x_train_AE_aug))
    y_train_AE = np.tile(y_train_AE,(len(noise)+1,1))

    # plot_multi_samples([y_train_AE[0,1:], y_rev[0,1:]], 
    #         ['y' + str(y_train_AE[0,0]),'yrev' + str(y_rev[0,0])])
    print('Augmented data AE: ', x_train_AE.shape, y_train_AE.shape)

if augment == 'DET':
    
    x_bias = x_train_DET.copy()
    y_bias = y_train_DET.copy()
    n = x_bias.shape[0]
   
    x_bias = np.tile(x_bias, (4, 1))
    y_bias = np.tile(y_bias, 4)
  
    print('bias', x_bias.shape)
    
    x_bias[n:2*n, 1:425] = x_bias[n:2*n, 1:425] - 0.1
    x_bias[2*n:3*n, 1:425] = x_bias[2*n:3*n, 1:425] + 0.1
    x_bias[3*n:4*n, 1:425] = x_bias[3*n:4*n, 1:425] - 0.17
    # plot_multi_samples([x_bias[30,:], x_bias[n+30,:], y_data[30,1:]], 
    #         ['y' + str(y_bias[30]),'ybias1' + str(y_bias[n+30]), 'yclean' ],save=False)

    x_train_DET = x_bias
    y_train_DET = y_bias

    x_rev = x_train_DET.copy()
    # y_rev = y_train_DET.copy()

    x_rev[:,:424] = np.flip(x_rev[:,:424], 1)
    x_rev[:,424:]  = np.flip(x_rev[:,424:], 1)

    # y_rev[:,:424] = np.flip(y_rev[:,:424], 1)
    # y_rev[:,424:]  = np.flip(y_rev[:,424:], 1)

    x_train_DET = np.vstack((x_train_DET, x_rev))
    y_train_DET = np.tile(y_train_DET,2)

    x_train_DET_aug = augment_data(x_train_DET, noise, augment)
    x_train_DET = np.vstack((x_train_DET, x_train_DET_aug))
    y_train_DET = np.tile(y_train_DET,len(noise)+1)

    print('Augmented data DET: ', 'y train:',len(y_train_DET),', x train:', x_train_DET.shape)

if augment == 'AEDET':
    x_train_AE = augment_data(x_train_AE, noise)
    y_train_AE = np.tile(y_train_AE,(len(noise),1))

    x_train_DET = augment_data(x_train_DET, noise)
    y_train_DET = np.tile(y_train_DET,(len(noise),1))

    print('Augmented data both: ', x_train_AE.shape, x_train_AE.shape)

    # plot_multi_samples([y_train_DET[0:2,1:], x_train_DET[0:2,1:]],['data', 'aug'], save=False)


################################################
# Scale data
################################################
if mode == 'quant_input':
    print('Applying quant transform on AE input')

    scaler_both = QuantileTransformer()
    scaled_data = scaler_both.fit_transform(x_train_AE[:,1:])
    print(scaled_data.shape)
    

    x_train_AE[:,1:] = scaled_data

    if save:
        dump(scaler_both, open(train_dir+'/scaler_both.pkl', 'wb'))  

    print('Post scale: ', x_train_AE.shape)

if mode == 'quant_camlid':
    print('Applying separate scale on cam and lid for AE')

    cam_scaler = QuantileTransformer()
    lid_scaler = QuantileTransformer()

    
    scaled_cam = cam_scaler.fit_transform(x_train_AE[:,1:425])
    scaled_lid = lid_scaler.fit_transform(x_train_AE[:,425:])


    x_train_AE[:,1:425] = scaled_cam
    x_train_AE[:,425:] = scaled_lid

    # plot_multi_samples([y_train_AE[0:2,1:], x_train_AE[0:2,1:]],['y data', 'scaled'], save=False)

    if save:
        dump(cam_scaler, open(train_dir+'/cam_scaler.pkl', 'wb'))  
        dump(lid_scaler, open(train_dir+'/lid_scaler.pkl', 'wb'))  

    print('Post scale: ', x_train_AE.shape)

if mode == 'unscaled':
    print('No scaling for AE')

################################################
# Dropout data
################################################

if dropout == 'AE':
    x_train_AE_temp = [x_train_AE]
    for perc in dropout_perc:
        x_train_AE_temp.append(dropout_data(x_train_AE, perc))

    x_train_AE = np.vstack(x_train_AE_temp)
    y_train_AE = np.tile(y_train_AE,(len(dropout_perc)+1,1))
    print('Dropout data AE: ', x_train_AE.shape, y_train_AE.shape)

if dropout == 'DET':
    x_train_DET_temp = []
    for perc in dropout_perc:
        x_train_DET_temp.append(dropout_data(x_train_DET, perc))

    x_train_DET = np.vstack(x_train_DET_temp)
    y_train_DET = np.tile(y_train_DET,(len(dropout_perc),1))
    print('Dropout data DET: ', x_train_DET.shape, y_train_DET.shape)

if dropout == 'AEDET':
    x_train_AE_temp = []
    x_train_DET_temp = []

    for perc in dropout_perc:
        x_train_AE_temp.append(dropout_data(x_train_AE, perc))
        x_train_DET_temp.append(dropout_data(x_train_DET, perc))

    x_train_AE = np.vstack(x_train_AE_temp)
    x_train_DET = np.vstack(x_train_DET_temp)

    y_train_AE = np.tile(y_train_AE,(len(dropout_perc),1))
    y_train_DET = np.tile(y_train_DET,(len(dropout_perc),1))
    print('Dropout data both: ', x_train_AE.shape, x_train_DET.shape)


    # plot_multi_samples([y_train_DET[0:2,1:], x_train_DET[0:2,1:]],['data', 'aug'], save=False)

################################################
# Save
################################################

if save:
    shuf_ind_AE = np.random.permutation(x_train_AE.shape[0])
    x_train_AE = x_train_AE[shuf_ind_AE, :]
    y_train_AE = y_train_AE[shuf_ind_AE, :]

    shuf_ind_DET = np.random.permutation(x_train_DET.shape[0])
    x_train_DET = x_train_DET[shuf_ind_DET, :]
    y_train_DET = y_train_DET[shuf_ind_DET]

    np.save(train_dir + '/orig_data.npy' ,orig_data)
    np.save(train_dir + '/y_data.npy' ,y_data)
    np.save(train_dir + '/x_train_AE.npy' ,x_train_AE)
    np.save(train_dir + '/y_train_AE.npy' ,y_train_AE)
    np.save(train_dir + '/x_train_DET.npy' ,x_train_DET)
    np.save(train_dir + '/y_train_DET.npy' ,y_train_DET)
    np.save(train_dir + '/settings.npy', settings) 