#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate
from keras.models import Model
import tensorflow as tf
from build_model import build_model
from keras.utils.vis_utils import plot_model
from keras.callbacks import EarlyStopping, ModelCheckpoint, LearningRateScheduler
from keras.utils import to_categorical
import keras
from pickle import load

print('Training of autoencoder')

# # Settings
# ###############################################################

net_dir = os.path.expanduser('~/new_network')
train_dir = os.path.expanduser('~/new_network/training')

settings = np.load(train_dir+'/settings.npy', allow_pickle=True).item() 


settings['layer_type'] = 'conv'  #'conv' or 'dense'
settings['filters'] = '5x5'  #
settings['size'] = 'big'  #'small' or 'big'
settings['bn'] = True      # True or False   
settings['n_epochs'] = 20
settings['loss_f'] = 'logcosh'    # 'mse' or 'mae' or 'kld' or 'cosine_similarity' or 'huber' or 'logcosh'
settings['lr_sch'] = 'constlr' 

settings_name = settings['size'] + '_' \
                + settings['filters'] + '_' \
                + str(settings['n_epochs']) + 'epochs_' \
                + settings['loss_f'] + '_' \
                + settings['lr_sch'] + '_' \
                + 'BN' + str(settings['bn']) + '_' \
                + settings['mode'] + '_' \
                + settings['dropout'] + '_' \
                + settings['augment']

# settings_name = settings['size'] + '_' \
#                 + settings['filters'] + '_' \
#                 + settings['mode'] + '_' \
#                 + settings['lr_sch'] + '_' \
#                 + settings['loss_f'] \
#                 + '_BN' + str(settings['bn']) + '_' \
#                 + str(settings['n_epochs']) \
#                 + 'epochs_' \
#                 + settings['dropout'] + '_' \
#                 + settings['augment']
                



print(settings)
print(settings_name)
np.save(train_dir+'/settings.npy', settings) 

# # Load data
# ###############################################################
# # Rightclick on file in tree to get relative or absolute path

x_train = np.load(train_dir+'/x_train_AE.npy')
y_train = np.load(train_dir+'/y_train_AE.npy')

x_train = x_train[:,1:]
y_train = y_train[:,1:]
print('train', x_train.shape, y_train.shape )


# Build model
###############################################################

model = build_model(settings['layer_type'], settings['size'], settings['bn'], 784)

# plot_model(model, to_file=net_dir+'/model_plots/'+settings['layer_type']+'_'+settings['size']+'_BN'+str(settings['bn'])+'.png', show_shapes=True, show_layer_names=True)


# # Training specifics
# ###############################################################

# Specify optimisers and loss functions for each output, and learningrate schedule

# Learning rate scheduler : https://keras.io/api/callbacks/learning_rate_scheduler/
def scheduler(epoch, lr):
    if epoch < 10:
        return lr
    else:
        return lr * tf.math.exp(-0.01)

lr_schedule = LearningRateScheduler(scheduler, verbose=1)


optimizer = tf.keras.optimizers.Adam(
    learning_rate=1e-2, beta_1=0.9, beta_2=0.999, epsilon=1e-06, amsgrad=False,
    name='Adam'
)


earlyStopping = EarlyStopping(monitor='val_loss', patience=settings['n_epochs'], verbose=0, mode='min')


mcp_save = ModelCheckpoint(net_dir + '/models/' + settings_name , 
                    save_best_only=True,
                    monitor='val_loss', 
                    mode='min', 
                    save_weights_only=False)



# # Train the model, and save metrics into history object
# ###############################################################

model.compile(optimizer=optimizer, 
        loss=settings['loss_f'],
        metrics=['mean_squared_error','mean_absolute_error', 'cosine_similarity', tf.keras.metrics.KLDivergence(name="kullback_leibler_divergence", dtype=None), tf.keras.metrics.LogCoshError(name="logcosh", dtype=None)])

history = model.fit(x_train, y_train, 
                    shuffle=True, epochs=settings['n_epochs'], verbose=2, batch_size=5000,          # Set verbose to 0 to supress all prints
                    validation_split = 0.2,
                    callbacks=[earlyStopping, mcp_save],        #add lr_schedule
                    max_queue_size=1,
                    use_multiprocessing=False, 
                    workers=1)




np.save(net_dir + '/history/' + settings_name,history.history)


