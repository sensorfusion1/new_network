#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os

map_dir = os.path.expanduser('~/new_network/eval/maps/')
tc = 'Aa'

# Room size = 4.35 x 3.70 meters
# Map resolution 0.05 (same as cartographer) meters/pixel
Y = 3.761 #kortsida mot dörrarna
X = 4.146 #långsida mot sängen
# origo hörnet mot skrivborder
res = 0.01

room = np.zeros((int(round(Y/res)), int(round(X/res))))

## room boundary
room[:, 0] = 1
room[:, -1] = 1
room[0, :] = 1
room[-1, :] = 1




objs = {        # x0, y0, width, height
    'wardrobe1': [3 , 1.748, 0.63, 0.96], #[3.464 , 1.748, 0.63, 0.96],
    'wardrobe2': [3.436 , 2.708, 0.658, 1.06],
    'bookshelf': [3.264, 0, 0.83, 0.305],
    'radiator': [0.192, 0, 1.13, 0.205],
    'desk1': [0.056, 0.24, 0.425, 0.6],
    'desk2': [1.045, 0.24, 0.425, 0.6],
    'leg1': [0.068, 1.785, 0.055, 0.055],
    'leg2': [1.133, 1.785, 0.055, 0.055],
    'leg3': [0.068, 3.651, 0.055, 0.055],
    'leg4': [1.133, 3.651, 0.055, 0.055],
    'blackbox': [2.161, 1.665, 0.34, 0.34]
}



for obj in objs:
    room[int(round((objs[obj][1]+res)/res)):int(round((objs[obj][1]+objs[obj][3]+res)/res)), 
                int(round((objs[obj][0]+res)/res)):int(round((objs[obj][0]+objs[obj][2]+res)/res))] = 1



plt.imsave(map_dir + 'gtruth_' + tc + '.png', room, cmap=cm.gray_r)


