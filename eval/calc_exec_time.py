#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

time_dir = os.path.expanduser('~/new_network/eval/exec_time/')

tc = 'Aa'

df = pd.read_csv(time_dir + tc + '.csv', sep='\t', header = 0)

plt.plot(df['Time'].values)

plt.show()