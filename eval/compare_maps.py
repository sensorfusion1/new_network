#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os
# import cv2
from skimage.io import imread
from skimage import data, img_as_float
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error as mse
import pathlib
from skimage.color import rgb2gray, rgba2rgb

map_dir = os.path.expanduser('~/new_network/eval/maps/')
tc = 'Aa'

truth = map_dir + 'gtruth_' + tc + '.png'
with_ae = map_dir + 'ae_' + tc + '.png'
without_ae = map_dir + 'no_ae_' + tc + '.png'

save_dir = os.path.expanduser('~/new_network/eval/compared_maps/')
save = True
pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True) 

def compare_images(imageA, imageB, imageC, title):
	# compute the mean squared error and structural similarity
	# index for the images
	m = mse(imageA, imageB)
	s = ssim(imageA, imageB)

	m2 = mse(imageA, imageC)
	s2 = ssim(imageA, imageC)
	# setup the figure
	fig = plt.figure(title)
	plt.suptitle("With AE - MSE: %.2f, SSIM: %.2f \n Without AE - MSE: %.2f, SSIM: %.2f " % (m, s, m2, s2))
	# show first image
	ax = fig.add_subplot(1, 3, 1)
	plt.imshow(imageA, cmap = plt.cm.gray)
	plt.title('Ground truth')
	plt.axis("off")
	# show the second image
	ax = fig.add_subplot(1, 3, 2)
	plt.imshow(imageB, cmap = plt.cm.gray)
	plt.title('With AE')
	plt.axis("off")

	ax = fig.add_subplot(1, 3, 3)
	plt.imshow(imageC, cmap = plt.cm.gray)
	plt.title('Without AE')
	plt.axis("off")
	# show the images
	plt.show()
	if save:
		plt.savefig(save_dir+title+'.jpg')


# load the images -- the original, the original + contrast,

ground_truth = imread(truth)
map_with_ae = imread(with_ae)
map_without_ae = imread(without_ae)

ground_truth = rgb2gray(rgba2rgb(ground_truth))
map_with_ae = rgb2gray(rgba2rgb(map_with_ae))
map_without_ae = rgb2gray(rgba2rgb(map_without_ae))


# compare the images
#compare_images(original, original, "Original vs. Original")
compare_images(ground_truth, map_with_ae, map_without_ae, tc)